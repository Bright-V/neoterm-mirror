#!/usr/bin/python3

import os
import time

template = ""
with open('template.htm') as f:
    template = f.read()

def KMG(byte):
    unit = 'B' 
    if byte > 1024:
        unit = 'K'
        byte /= 1024
    if byte > 1024:
        unit = 'M'
        byte /= 1024
    if byte > 1024:
        unit = 'G'
        byte /= 1024

    return "%.2f %s" % (byte, unit)
        

def gen_index(dir):
    html = ""
    tbody = ('<tr><td><a href="' + '../index.html' + '">' +'../'+ '</a></td>'+
             '<td align="right">' + '-' + '</td><td align="center">' + '-'  + '</td></tr>\n'
            )
    files = []
    for f in os.listdir(dir):
        if f[0] != '.' and f != 'index.html':
            path = dir + f
            if os.path.isdir(path):
                path += '/'
                size = '-'
                date = time.asctime( time.localtime(os.stat(dir +f).st_mtime))
                
                tbody += (
                    '<tr><td><a href="' +f+ '/index.html">' +f+ '/</a></td>'+
                    '<td align="right">' + size + '</td><td align="center">' + date + '</td></tr>\n'
                )

                gen_index(path)
            else:
                files.append(f)

    for f in files:
        size = KMG(os.stat(dir + f).st_size)
        date = time.asctime( time.localtime(os.stat(dir +f).st_mtime))
        tbody += (
            '<tr><td><a href="' +f+ '">' +f+ '</a></td>'+
            '<td align="right">' + size + '</td><td align="center">' + date + '</td></tr>\n'
        )

    
    html = template.replace('%tbody%',tbody).replace('%title%','index of '+ dir[1:])

    with open(dir+'index.html','w') as f:
        f.write(html)
        print ('gen '+ dir + 'index.html')


gen_index('./')




