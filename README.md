# neoterm 的备用软件源

由于官方原已挂，，因此搭建备用源

### 源地址

```
https://lcron.coding.net/p/neoterm-mirror/d/neoterm-mirror/git/raw/master
```

**注意：** 直接访问源地址或目录是404，这是正常的，因为raw接口不会返回index.html文件，只能访问文件，不能访问目录，不过可以在本页打开文件夹以预览文件。 

**本页面不是源地址** ，只是使用指南和文件列表。


# 使用

复制以下到终端执行

```shell
sed -i 's@^\(deb.*stable main\)$@#\1\ndeb https://lcron.coding.net/p/neoterm-mirror/d/neoterm-mirror/git/raw/master  stable main@' /data/data/io.neoterm/files/usr/etc/apt/sources.list
apt update && apt upgrade
```

**或者**

编辑 /data/data/io.neoterm/files/usr/etc/apt/sources.list 修改为如下内容

```shell
deb https://lcron.coding.net/p/neoterm-mirror/d/neoterm-mirror/git/raw/master stable main
```

然后执行 `apt update`

# 备份

本软件源是以git仓库的方式托管在 [coding](https://coding.net)，我不需要出任何服务器资源，**理论上** 永久能稳定使用，但说不定什么时候coding停止服务或删除本仓库，建议克隆本仓库并进行备份，以备不时之需。


### 克隆本仓库

```
git  clone https://e.coding.net/lcron/neoterm-mirror/neoterm-mirror.git 
```

### 打包上传

文件过多不方便传输，需要打包一下，这里使用常用的zip来打包压缩

```
zip -r neoterm-mirror.zip neoterm-mirror/*
```

然后，把文件上传到自己常用的云储存服务。

### git 推送

也可以推送到git托管服务，推荐使用 [coding](https://coding.net) 或 [gitlab](https://gitlab.com)，这些服务一般都有raw接口，这样也可以自己搭建源。

NOTE  *github 使用了 lfs 还会限制 1G 大小，可能我我操作上有问题，大家可以自己试一下。*


# 备注
本源拷贝自 [covariant](http://neoterm.covariant.cn/)，在官方源或covariant源更新之前，本源不会添加或更新任何软件。如果需要最新版本的软件，请用其它方式安装，比如直接从源代码编译安装。

# 许可

[CC0](LICENSE)


